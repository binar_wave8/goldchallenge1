const express = require('express');
const app = express();
const { Items } = require('../models');
const router = express.Router();

const insert = async (req, res) => {
    try {
        Items.create({
            name: req.body.name,
            description: req.body.description,
            price: req.body.price
        }).then(value => {
            return res.status(200).json({
                status: 200,
                data: value
            });
        })
    } catch (error) {
        return res.status(500).json(`server error ${error}`)
    }
}

const getAll = async (req, res) => {
    try {
        Items.findAll().then(value => {
            return res.status(200).json(value);
        })
    } catch (error) {
        return res.status(500).json(`server error ${error}`)
    }
}

const getByID = async (req, res) => {
    try {
        console.log(req.params.id)
        Items.findAll({
            where: {
                id: req.params.id
            }
        }).then(value => {
            return res.status(200).json(value);
        })
    } catch (error) {
        return res.status(500).json(`server error ${error}`)
    }
}

const update = async (req, res) => {
    try {
        Items.update({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email
        }).then(value => {
            return res.status(200).json(value);
        })
    } catch (error) {
        return res.status(500).json(error);
    }
}

const deleted = async (req, res) => {
    try {
        Items.delete({
            id: req.params.id
        }).then(value => {
            return res.status(200).json({ msg: 'terhapus' });
        })
    } catch (error) {
        return res.status(500).json(error);
    }
}


module.exports = { insert, getAll, update, deleted, getByID };
