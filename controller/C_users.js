const express = require('express');
const app = express();
// const crypto = require('crypto');
const { User } = require('../models');
const router = express.Router();

// app.use(express.json());

const register = async (req, res) => {
    try {
        User.create({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email
        }).then(value => {
            return res.status(200).json(value);
        })
    } catch (error) {
        return res.status(500).json(`server error ${error}`)
    }
}

const getAll = async (req, res) => {
    try {
        User.findAll().then(value => {
            return res.status(200).json(value);
        })
    } catch (error) {
        return res.status(500).json(`server error ${error}`)
    }
}

const getByID = async (req, res) => {
    try {
        console.log(req.params.id)
        User.findAll({
            where: {
                id: req.params.id
            }
        }).then(value => {
            return res.status(200).json(value);
        })
    } catch (error) {
        return res.status(500).json(`server error ${error}`)
    }
}

const update = async (req, res) => {
    try {
        User.update({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email
        }).then(value => {
            return res.status(200).json(value);
        })
    } catch (error) {
        return res.status(500).json(error);
    }
}

const deleted = async (req, res) => {
    try {
        User.delete({
            id: req.params.id
        }).then(value => {
            return res.status(200).json({ msg: 'terhapus' });
        })
    } catch (error) {
        return res.status(500).json(error);
    }
}


module.exports = { register, getAll, update, deleted, getByID };
