const router = require('express').Router();
const Clogin = require('../controller/C_login')
const route = require('../controller/C_users');

router
    .post('/login', Clogin.login)
    .post('/users/add', route.register)
    .get('/users', route.getAll)
    .get('/users/:id', route.getByID)
    .put('/users/:id', route.update)
    .delete('/users/delete/:id', route.deleted);



module.exports = router;