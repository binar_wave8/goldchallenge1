const express = require('express');
const app = express();
const { Orders } = require('../models');
const router = express.Router();
const { User, Items } = require('../models')

const insert = async (req, res) => {
    // console.log('wkwkwk');
    try {
        console.log(Date.now());
        Orders.create({
            user_id: req.body.user_id,
            item_id: req.body.item_id,
            order_date: Date.now(),
            quantity: req.body.quantity,
            status: req.body.status
        }).then(value => {
            return res.status(200).json({
                status: 200,
                data: value
            });
        })
    } catch (error) {
        return res.status(500).json(`server error ${error}`)
    }
}

const getAll = async (req, res) => {
    try {
        Orders.findAll({
            include: [
                {
                    model: User
                },
                {
                    model: Items
                }
            ]
        }).then(value => {
            return res.status(200).json(value);
        })
    } catch (error) {
        return res.status(500).json(`server error ${error}`)
    }
}

const getByID = async (req, res) => {
    try {
        console.log(req.params.id)
        Orders.findAll({
            include: [
                {
                    model: User
                },
                {
                    model: Items
                }
            ],
            where: {
                id: req.params.id
            }
        }).then(value => {
            return res.status(200).json(value);
        })
    } catch (error) {
        return res.status(500).json(`server error ${error}`)
    }
}

// const update = async (req, res) => {
//     try {
//         Orders.update({
//             firstName: req.body.firstName,
//             lastName: req.body.lastName,
//             email: req.body.email
//         }).then(value => {
//             return res.status(200).json(value);
//         })
//     } catch (error) {
//         return res.status(500).json(error);
//     }
// }

// const deleted = async (req, res) => {
//     try {
//         Orders.delete({
//             id: req.params.id
//         }).then(value => {
//             return res.status(200).json({ msg: 'terhapus' });
//         })
//     } catch (error) {
//         return res.status(500).json(error);
//     }
// }


module.exports = { insert, getAll, getByID };
