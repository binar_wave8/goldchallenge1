const express = require('express');
const rUser = require('./routes/user');
const rItem = require('./routes/items');
const rOrder = require('./routes/order');
const app = express()
const port = 5050


app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use(rItem);
app.use(rUser);
app.use(rOrder);


app.listen(port, () => {
    console.log(`berjalan di server ${port}`);
})