const router = require('express').Router();
const order = require('../controller/C_orders');

router
    .post('/order/add', order.insert)
    .get('/order', order.getAll)
    .get('/order/:id', order.getByID)



module.exports = router;