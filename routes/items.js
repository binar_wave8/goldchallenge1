const router = require('express').Router();
const item = require('../controller/C_items')

router
    .post('/items/add', item.insert)
    .get('/items', item.getAll)
    .get('/items/:id', item.getByID)
    .put('/items/:id', item.update)
    .delete('/items/delete/:id', item.deleted);



module.exports = router;